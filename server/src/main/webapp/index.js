$(document).ready(function() {

    $('#logo').click(function() {
        location.reload();
    });

    $('#connexion').click(function() {
        $('#conteneurAccueil').hide();
        $('#onglet').show();
        contenu.innerHTML = identificationPage;
        active(ong2);
        document.body.style.backgroundColor = "#354665";
    });

    $('#inscription').click(function() {
        $('#conteneurAccueil').hide();
        $('#onglet').show();
        contenu.innerHTML = inscriptionPage;
        active(ong1);
        document.body.style.backgroundColor = "#354665";
    });

    $('#deconnexion').click(function() {
        $('#loginUser').val("");
        $('#connexion').show();
        $('#inscription').show();
        $('#profil').hide();
        $('#deconnexion').hide();
        $('#moderation').hide();
        $('#show_mode').hide();
        $('#show_services').hide();
        location.reload();
    });

    $('#show_profil').hide();
    $('#onglet').hide();
    $('#deconnexion').hide();
    $('#profil').hide();
    $('#show_services').hide();
    $('#moderation').hide();
    $('#mode').hide();
    $('#show_mode').hide();
    $('.buttons_mode').hide();

    $('#explication_precis1').animate({height: '150px', opacity: '0.4'}, "slow");
    $('#explication_precis2').animate({height: '150px', opacity: '0.4'}, "slow");
    $('#explication_precis3').animate({height: '150px', opacity: '0.4'}, "slow");
    $('#explication_precis1').animate({height: '150px', opacity: '1'}, "slow");
    $('#explication_precis2').animate({height: '150px', opacity: '1'}, "slow");
    $('#explication_precis3').animate({height: '150px', opacity: '1'}, "slow");
});
var URL = "http://51.255.131.196:80";
function checkValue(element){
    var champ = document.getElementById(element);
    if(champ.value == 'Recherche..'){
        champ.value = '';
    }else{
        champ.value = 'Recherche..';
    }
}

ong1 = document.getElementById('inscription_login_page');
ong2 = document.getElementById('connexion_login_page');
contenu = document.getElementById('content_login_page');

const identificationPage = "<div class='container'><div class='row centered-form'><div class='col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4'><div class='panel'><div class='panel-body'><div class='form-group'><input type='text' name='login' id='login' class='form-control input-sm' placeholder='Address mail'></div><div class='form-group'><input type='password' name='pass' id='pass' class='form-control input-sm' placeholder='password'></div><button type='button' onClick='connexion()' name='buttonConnexion' class='btn btn-info btn-block'>Connexion</button></div></div></div></div></div>";

const inscriptionPage = "<div class='container'><div class='row centered-form'><div class='col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4'><div class='panel'><div class='panel-body'><div class='row'><div class='col-xs-6 col-sm-6 col-md-6'><div class='form-group'><input type='text' name='first_name' id='first_name' class='form-control input-sm floatlabel' placeholder='First Name'></div></div><div class='col-xs-6 col-sm-6 col-md-6'><div class='form-group'><input type='text' name='last_name' id='last_name' class='form-control input-sm' placeholder='Last Name'></div></div></div><div class='row'><div class='col-xs-6 col-sm-6 col-md-6'><div class='form-group'><input type='text' name='numero' id='numero' class='form-control input-sm floatlabel' placeholder='Number'></div></div><div class='col-xs-6 col-sm-6 col-md-6'><div class='form-group'><select name='type' id='type' class='form-control'><option>etudiant</option><option>senior</option></select></div></div></div><div class='form-group'><div class='form-group'><input type='email' name='login' id='login' class='form-control input-sm' placeholder='Address Mail'></div><div class='row'><div class='col-xs-6 col-sm-6 col-md-6'><div class='form-group'><input type='password' name='password' id='password' class='form-control input-sm' placeholder='Password'></div></div><div class='col-xs-6 col-sm-6 col-md-6'><div class='form-group'><input type='password' name='password_confirmation' id='password_confirmation' class='form-control input-sm' placeholder='Confirm Password' onfocus='checkMdp()' onblur='checkMdp()'></div></div></div><button type='button' onClick='inscription()' name='buttonInscription' class='btn btn-info btn-block'>Inscription</button></div></div></div></div></div>";
contenu.innerHTML = identificationPage;

function checkMdp(){
    if($('#password').val() == $('#password_confirmation').val() && $('#password').val().length > 5 ){
        $('#password_confirmation').css('border','3px solid green');
    } else {
        $('#password_confirmation').css('border','3px solid red'); 
    }
}

function nonactive(){
    ong1.className = "";
    ong2.className = "";
}

function active(moi){
    nonactive(); // nettoyage
    moi.className="active"; // je deviens active
}

ong1.addEventListener("click",function(){
    contenu.innerHTML = inscriptionPage;
    active(this);
})

ong2.addEventListener("click",function(){
    contenu.innerHTML = identificationPage;
    active(this);
})



function inscription(){

    if($('#password').val() == $('#password_confirmation').val() && $('#password').val().length > 5 ){   
        var nom=document.getElementById("last_name").value;
        var prenom=document.getElementById("first_name").value;
        var numero=document.getElementById("numero").value;
        var login=document.getElementById("login").value;
        var mdp=document.getElementById("password").value;
        var planning=null;
        var statut=document.getElementById("type").value;


        var regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$','i');

        if(!regEmail.test(login)){

            alert("Adresse mail incorrect !");
            $('login').css('border','3px solid red'); 
        } else {


            <!--changer le type en menu deroulant (senior/etudiant)-->
                var url="v1/user/"; 
            $.ajax({
                type : 'POST',
                contentType : 'application/json',
                url : url,
                dataType : "json",
                data : JSON.stringify({
                    "nom" : nom,
                    "prenom" : prenom,
                    "login" : login,
                    "numero" : numero,
                    "statut" : statut,
                    "password":mdp
                }),
                success : function(data, textStatus, jqXHR) {
                    alert("Profil créé avec succés. Il sera accessible lorsqu'il aura été validé par le modérateur.");
                    document.body.style.backgroundColor = "white";
                    $('#conteneurAccueil').show();
                    $('#onglet').hide();
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert("Erreur lors de la création de compte. Le login choisi peut-être déjà utilisé.");
                    console.log("jqXHR" +jqXHR);
                    console.log("error"+errorThrown);
                    console.log('postUser error: ' + textStatus);
                }
            });
        }
    } else if ($('#password').val().length <= 5){
        alert("Le mot de passe doit contenir au moins 6 caractéres");
    } else {
        alert("Mot de passe incorrect !");

    }

}

function connexion(){

    var login=document.getElementById("login").value;
    var mdp=document.getElementById("pass").value;


    var url="v1/user/"+login; 
    $.ajax({
        type : 'GET',
        url : url,
        dataType : "json",
        beforeSend : function(req) {
            req.setRequestHeader("Authorization", "Basic " + btoa(login + ":" + mdp));
        },

        success : function(json) {
            var tab = JSON.stringify(json);
            var js=JSON.parse(tab);
            var valide=js.valide;
            console.log(valide);
            if(valide == "oui"){
                alert("Connexion OK !");
                $('#loginUser').val(login);
                document.body.style.backgroundColor = "white";
                $('#conteneurAccueil').show();
                $('#connexion').hide();
                $('#inscription').hide();
                $('#profil').show();
                $('#deconnexion').show();
                $('#onglet').hide();
                if(login == "admin@admin.com"){
                    $('#moderation').show();
                }
            } else {
                alert("Votre profil n'a pas encore été validé par le modérateur.");
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert("Login ou mot de passe incorrect !")
            console.log('postUser error: ' + textStatus);
        }
    });
}

function generate_login_page(){
    $('#conteneurAccueil').hide();
    $('#show_services').hide();
    $('#moderation').hide();
    $('#show_mode').hide();
    $('#show_profil').hide();
    $('#show_profil').show();
    init_page_login_whit_get();
    get_all_champs();
}

var labels = ["statut", "email", "nom", "prenom", "numero"];
var div_init = document.getElementById('init_div');

function init_page_login_whit_get(){
    var page_de_base = "";
    for(var i=0; i<labels.length; i++) {
        if(i <= 1){
            page_de_base += "<div class='inline_profil'><label class='label_profil'>" + labels[i] +" : </label><div id='div_" + labels[i] + "_get' class='div_profil_get'></div></div><br/><br/>"
        } else if(i === labels.length-1){
            page_de_base += "<div class='inline_profil'><label class='label_profil'>" + labels[i] +" : </label><div id='div_" + labels[i] + "_get' class='div_profil_get' style='display:none'></div><div id='div_" + labels[i] + "' class='div_profil'><button class='button_profil' onclick=\"display_input_update('" + labels[i] + "')\">Modifier</button></div></div><br/><br/>"
        } else {
            page_de_base += "<div class='inline_profil'><label class='label_profil'>" + labels[i] +" : </label><div id='div_" + labels[i] + "_get' class='div_profil_get'></div><div id='div_" + labels[i] + "' class='div_profil'><button class='button_profil' onclick=\"display_input_update('" + labels[i] + "')\">Modifier</button></div></div><br/><br/>"
        }
    }             
    page_de_base += "<br/><button class='button_profil' onclick=\"send_update()\">Confirmer et envoyer les modifications</button>"
    div_init.innerHTML = page_de_base;
}

function get_all_champs(){
    var login=$('#loginUser').val();
    var url="v1/user/"+login; 
    console.log(url);
    console.log("login: "+login)
    console.log("TEST1")
    $.ajax({
        type : 'GET',
        url : url,
        dataType : "json",
        success : function(json) {
            var tab = JSON.stringify(json);
            var js=JSON.parse(tab);console.log(js);
            $('#div_statut_get').text(js.statut)
            $('#div_nom_get').text(js.nom)
            $('#div_prenom_get').text(js.prenom)
            $('#div_numero_get').text(js.numero)
            $('#div_email_get').text(js.login)
            $('#div_password_get').text(js.password)
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert("Login incorrect !")
            console.log('postUser error: ' + textStatus);
        }
    });
}

function display_input_update(param){                
    var display = ""
    var div = "div_" + param
    var div_display = document.getElementById(div)
    var html_display = ""

    if (param === "numero"){
        html_display = "<input type='tel' name='" + param + "' id='" + param + "_input' placeholder='" + param + "' class='input_profil' required><button class='button_profil' onclick=\"display_update('" + param + "')\">Envoyer</button>"
    } else {
        html_display = "<input type='text' name='" + param + "' id='" + param + "_input' placeholder='" + param + "' class='input_profil' required><button class='button_profil' onclick=\"display_update('" + param + "')\">Envoyer</button>"
    }
    div_display.innerHTML = html_display;
}

function display_update(param){
    var div_name = "div_" + param + "_get"
    var div_update_put = document.getElementById(div_name);
    var input_changes = $("#"+param+"_input").val()
    $("#"+div_name).text(input_changes)
}

function send_update(){
    var url="v1/user/"+$('#loginUser').val(); 
    $.ajax({
        type: 'PUT',
        url: url,
        contentType : 'application/json',
        data : JSON.stringify({
            "nom":$("#div_nom_get").text(),
            "prenom":$("#div_prenom_get").text(),
            "numero":$("#div_numero_get").text()           
        }),

        dataType : "json",
        success: function( json ) {
            alert("Modification effectuées");
        },
        error: function( xhr, status, errorThrown ) {

            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        },
        complete: function( xhr, status ) {

        }
    });
}

function create_services(param){
    $('#conteneurAccueil').hide();
    $('#show_services').show();

    var services_init = document.getElementById('show_services');

    var html_services = "<h1>"+param+"</h1><br/><iframe name='InlineFrame1' id='InlineFrame1' style='width:180px;height:220px;' src='https://www.mathieuweb.fr/calendrier/calendrier-des-semaines.php?nb_mois=1&nb_mois_ligne=4&mois=&an=&langue=fr&texte_color=B9CBDD&week_color=DAE9F8&week_end_color=C7DAED&police_color=453413&sel=true' scrolling='no' frameborder='0' allowtransparency='true'></iframe>";
    services_init.innerHTML = html_services;
}

$('#moderation').click(function() {    
    $('#conteneurAccueil').hide();
    $('#show_services').hide();
    $('#moderation').hide();
    $('#show_mode').hide();
    $('#moderation').show();
    $('#show_mode').show();
});

function listeUsers(){
    $.ajax({
        url: URL+"/v1/user/!valide",
        type: "GET",
        dataType : "json",
        success: function( json ) {
            var tab = JSON.stringify(json);
            var js=JSON.parse(tab);
            var res="<h1>Utilisateurs en attente</h1><table id=\"tableUser\"><tr><th><a  href=\"javascript:sortTable('tableUser',0, 'asc')\" value=\"tri alphanum asc col1\">Nom</th><th><a  href=\"javascript:sortTable('tableUser',1, 'asc')\" value=\"tri alphanum asc col1\">Prenom</th><th><a  href=\"javascript:sortTable('tableUser',2, '?asc')\" value=\"tri alphanum asc col1\">Login</th><th><a  href=\"javascript:sortTable('tableUser',3, '?asc')\" value=\"tri alphanum asc col1\">Numero</th><th><a  href=\"javascript:sortTable('tableUser',4, '?asc')\" value=\"tri alphanum asc col1\">Statut</th><th>Validation</th></tr>";
            for(var cpt=0;cpt<js.length;cpt++){
                res=res+"<tr class=\"ligne\"><td>"+js[cpt].nom +"</td>"+"<td>"+js[cpt].prenom+"</td>"+"<td id=\"log\">"+js[cpt].login+"</td><td>"+js[cpt].numero+"</td><td>"+js[cpt].statut+"</td><td><button type=\"button\"  id='but' onClick=\"validate('"+js[cpt].login+"')\" >Valider</button></td></tr>";
            }
            $('#show_mode_options').html(res);
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        },
        complete: function( xhr, status ) {
        }
    });

}

function validate(login){
    $.ajax({
        url: URL+"/v1/user/validate/" + login,
        type: "PUT",
        success: function(data) {
            alert('Utilisateur Validé');
            listeUsers();
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        }
    });
}

function listValidUsers(){
    $.ajax({
        url: URL+"/v1/user/valide",
        type: "GET",
        dataType : "json",
        success: function( json ) {
            var tab = JSON.stringify(json);
            var js=JSON.parse(tab);
            var res="<h1>Liste des Utilisateurs</h1><table id=\"tableUser\"><tr><th><a  href=\"javascript:sortTable('tableUser',0, 'asc')\" value=\"tri alphanum asc col1\">Nom</th><th><a  href=\"javascript:sortTable('tableUser',1, 'asc')\" value=\"tri alphanum asc col1\">Prenom</th><th><a  href=\"javascript:sortTable('tableUser',2, '?asc')\" value=\"tri alphanum asc col1\">Login</th><th><a  href=\"javascript:sortTable('tableUser',3, '?asc')\" value=\"tri alphanum asc col1\">Numero</th><th><a  href=\"javascript:sortTable('tableUser',4, '?asc')\" value=\"tri alphanum asc col1\">Statut</th><th>Gestion</th></tr>";
            for(var cpt=0;cpt<js.length;cpt++){
                res=res+"<tr class=\"ligne\"><td>"+js[cpt].nom +"</td>"+"<td>"+js[cpt].prenom+"</td>"+"<td id=\"log\">"+js[cpt].login+"</td><td>"+js[cpt].numero+"</td><td>"+js[cpt].statut+"</td><td><button type=\"button\" id='but' onClick=\"suppression('"+js[cpt].login+"')\" >Supprimer</button><button type=\"button\" id='but' onClick=\"suppressValid('"+js[cpt].login+"')\" >Invalider</button></td></tr>";
            }
            $('#show_mode_options').html(res);
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        },
        complete: function( xhr, status ) {
        }
    });
}

function suppression(login){
    $.ajax({
        url: URL+"/v1/user/" + login,
        type: "DELETE",
        success: function(data) {
            alert('Utilisateur Supprimé');
            listValidUsers();
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        }
    });
}

function suppressValid(login){
    $.ajax({
        url: URL+"/v1/user/!validate/" + login,
        type: "PUT",
        success: function(data) {
            alert('Utilisateur Invalidé');
            listValidUsers();
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        }
    });
}

function sortTable(tid, col, ord){
    var mybody = $("#"+tid).children('tbody'),
        lines = mybody.children('.ligne'),
        sorter = [],
        i = -1,
        j = -1;            
    while(lines[++i]){
        sorter.push([lines.eq(i), lines.eq(i).children('td').eq(col).text()])
    }            if (ord == 'asc') {
        sorter.sort();
    } 
    else if (ord == 'desc') {
        sorter.sort().reverse();
    } 
    else if (ord == 'nasc'){
        sorter.sort(function(a, b){return(a[1] - b[1]);});
    } 
    else if (ord == 'ndesc'){
        sorter.sort(function(a, b){return(b[1] - a[1]);});
    } 
    else if (ord == '?asc'){
        sorter.sort(function(a, b){
            var x = parseInt(a[1], 10),
                y = parseInt(b[1], 10);                    
            if (isNaN(x) || isNaN(y)){
                if (a[1] > b[1]){
                    return 1;
                } else if(a[1] < b[1]){
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return(a[1] - b[1]);
            }
        });
    }
    else if (ord == '?desc'){
        sorter.sort(function(a, b){
            var x = parseInt(a[1], 10),
                y = parseInt(b[1], 10);                    
            if (isNaN(x) || isNaN(y)){
                if (a[1] > b[1]){
                    return -1;
                } else if(a[1] < b[1]){
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return(b[1] - a[1]);
            }
        });
    } 
    while(sorter[++j]){
        mybody.append(sorter[j][0]); 
    }
}

function listRdv(){
    $.ajax({
        url: URL+"/v1/rdv",
        type:"GET",
        dataType :"json",
        success:function(json){
            var tab = JSON.stringify(json);
            var js = JSON.parse(tab);
            var res="<h1>Liste des rendez-vous</h1><table id=\"tableRDV\"><tr><th><a  href=\"javascript:sortTable('tableRDV',0, 'asc')\" value=\"tri alphanum asc col1\">Senior</th><th><a  href=\"javascript:sortTable('tableRDV',1, 'asc')\" value=\"tri alphanum asc col1\">Etudiant</th><th><a  href=\"javascript:sortTable('tableRDV',2, '?asc')\" value=\"tri alphanum asc col1\">Jour</th><th><a  href=\"javascript:sortTable('tableRDV',3, '?asc')\" value=\"tri alphanum asc col1\">Service </th><th><a  href=\"javascript:sortTable('tableRDV',4, '?asc')\" value=\"tri alphanum asc col1\">Matin</th><th><a  href=\"javascript:sortTable('tableRDV',5, '?asc')\" value=\"tri alphanum asc col1\">Aprem</th><th><a  href=\"javascript:sortTable('tableRDV',6, '?asc')\" value=\"tri alphanum asc col1\">Soir</th><th>Ajout Etudiant</th></tr>";
            for(var cpt=0;cpt<js.length;cpt++){
                var moment = function(){                   }
                res=res+"<tr class=\"ligne\"><td>"+js[cpt].senior +"</td>"+"<td>"+js[cpt].etudiant+"</td>"+"<td>"+js[cpt].jour+"</td><td>"+js[cpt].service+"</td><td>"+js[cpt].matin+"</td><td>"+js[cpt].aprem+"</td><td>"+js[cpt].soir+"</td>";
                if(js[cpt].etudiant==""){
                    res=res+"<td><button type=\"button\" id='but' onClick=\"ajoutEtu('"+js[cpt].senior+"')\" >Ajouter un étudiant</button></td>";
                }else{
                    res=res+"<td>Un étudiant est déjà attribué";
                }
                res=res+"</tr>"
            }
            $('#show_mode_options').html(res);
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        },
        complete: function( xhr, status ) {
        }
    });
}

function ajoutEtu(login){
    $.ajax({
        url: URL+"/v1/user",
        type:"GET",
        dataType :"json",
        success: function( json ) {
            var tab = JSON.stringify(json);
            var js=JSON.parse(tab);
            var res="<h1>Liste des Utilisateurs</h1><table id=\"tableUser\"><tr><th><a href=\"javascript:sortTable('tableUser',0, 'asc')\" value=\"tri alphanum asc col1\">Nom</th><th><a href=\"javascript:sortTable('tableUser',1, 'asc')\" value=\"tri alphanum asc col1\">Prenom</th><th><a href=\"javascript:sortTable('tableUser',2, '?asc')\" value=\"tri alphanum asc col1\">Login</th><th><a href=\"javascript:sortTable('tableUser',3, '?asc')\" value=\"tri alphanum asc col1\">Numero</th><th><a href=\"javascript:sortTable('tableUser',4, '?asc')\" value=\"tri alphanum asc col1\">Email</th><th>Ajouter</th></tr>";
            for(var cpt=0;cpt<js.length;cpt++){
                // if(js[cpt].status=="Etudiant"){
                res=res+"<tr class=\"ligne\"><td>"+js[cpt].nom +"</td>"+"<td>"+js[cpt].prenom+"</td>"+"<td id=\"log\">"+js[cpt].login+"</td><td>"+js[cpt].numero+"</td><td>"+js[cpt].email+"</td><td><button type=\"button\" id='but' onClick=\"ajoutEtuFinal('"+login+","+js[cpt].jour+","+js[cpt].matin+","+js[cpt].aprem+","+js[cpt].soir+","+js[cpt].service+","+js[cpt].login+"')\" >Ajouter l'étudiant</button></td></tr>";
                // }
            }
            $('#show_mode_options').html(res);
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        },
        complete: function( xhr, status ) {
        }
    });
}

function ajoutEtuFinal(login, jour, matin, aprem, soir, service, loginEtu){
    $.ajax({

        url: URL+"/v1/rdv/validate/"+login+"\&"+jour+"\&"+jour+"\&"+matin+"\&"+aprem+"\&"+soir+"\&"+service+"\&"+loginEtu,
        type:"PUT",
        success: function(data) {
            alert('Utilisateur Validé');
            listRdv();
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        }
    });
}

function listService(){
    $.ajax({
        url: URL+"/v1/service",
        type: "GET",
        dataType : "json",
        success: function( json ) {
            var tab = JSON.stringify(json);
            var js=JSON.parse(tab);
            var res="<h1>Liste des Services</h1><table id=\"tableService\"><tr><th><a  href=\"javascript:sortTable('tableUser',0, 'asc')\" value=\"tri alphanum asc col1\">Libellé</th><th>Gestion</th></tr>";
            for(var cpt=0;cpt<js.length;cpt++){
                res=res+"<tr class=\"ligne\"><td>"+js[cpt].libelle +"</td>"+"<td><button type=\"button\" id='but' onClick=\"suppressionService('"+js[cpt].libelle+"')\" >Supprimer</button></td></tr>";
            }
            $('#show_mode_options').html(res);
        }
    });
}

function ajoutService(libelle){
    $.ajax({
        url: URL+"/v1/rdv/" + id + "/" + login,
        type: "PUT",
        success: function(data) {
            alert('Utilisateur Validé');
            listRdv();
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        }
    });
}

function suppressionService(libelle){
    $.ajax({
        url: URL+"/v1/service/" + libelle,
        type: "DELETE",
        success: function(data) {
            alert('Service Supprimé');
            listService();
        },
        error: function( xhr, status, errorThrown ) {
            alert( "Sorry, there was a problem!" );
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
            console.dir( xhr );
        }
    });
}